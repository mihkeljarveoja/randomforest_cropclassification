## Põllukultuuride tuvastamise masinõppe mudeli andmete eeltöötlus, sobitamine ja tunnuste olulisuse hindamine

Tegemist on Tartu ülikooli magistritöö "Põllukultuuride tuvastamise masinõppe mudeli tunnuste olulisuse hindamine" raames tehtud andmete eeltöötluse, juhumetsa mudeli sobitamise ja tunnuste olulisuse hindamise lähtekoodiga.
Lähtekoodi avalikustamise peamiseks eesmärgiks on demonstreerida tehtud tööd ja loodud funktsioone. Kogu protsessi kordamiseks on vaja taastada sisendandmestiku andmebaas, mis on kättesaadav RITA Kaugseire projekti raames loodud andmekogus: https://datadoi.ee/handle/33/316.
Magistritöö autor: Mihkel Järveoja (mihkeljarveoja@gmail.com). Töö kaitstakse Tartu ülikooli arvutiteaduse instituudis 2021. aasta kevadel. Nii magistritöö kui lähtekood on tehtud kättesaadavaks Creative Commonsi litsentsiga CC BY NC ND 3.0.

---

## Peamised vajalikud moodulid ja teegid
1. **python** >= 3.6.10.
2. **psycopg2** >= 2.8.4.
3. **pandas** >= 1.1.5.
4. **scikit-learn** >= 0.22.1.
5. **numpy** >= 1.18.1.
6. **seaborn** >= 0.9.0.

## Failide jaotus
Kogu lähtekood koosneb kahest .py failist failist.

1. **preprocessing.py** - Andmete pärimine andmebaasist ja nende eeltöötlus kuni .npz faili loomiseni, mis sisaldab näidiseid ja märgendeid ning on mudeli sobitamise etapi sisendiks.
2. **fitting_predicting.py** - Näidiseid ja märgendeid sisaldava .npz faili laadimisest kuni klassifikatsiooni ja tunnuste olulisuse hindamise tulemuste visualiseerimiseni.  

## Seadistamine
Eraldi seadistusfaili pole, mistõttu soovitud muudatuste puhul tuleb muuta lähtekoodi järgmistes kohtades:

1. Andmebaasi poole pöördumise parameetreid preprocessing.py failis. 
2. Päritavate tunnuste aegridade ulatust sql päringutes preprocessing.py failis. 
3. Väljundfailide nimesid nii preprocessing.py kui fitting_predicting.py failis.

## Väljundid
preprocessing.py jooksutamise väljundid:

1. processing_log.csv - logifail.
2. train_set.npz - korrastatud treeningandmestik, mis sisaldab näidiste/tunnuste massiivi koos märgendite massiiviga.
3. train_set_headers.csv - tunnuste nimetused.

fitting_predicting.py jooksutamise väljundid:

1. classification_report.txt - treeningandmestiku statistika, testkogul klassifitseerimise mõõdikud.
2. confusion_matrix.csv ja confusion_matrix_norm.csv - testkogu klassifitseerimise eksimismaatriksi toorandmed normeerimata ja normeeritud kujul .
3. confusion_matrix.png - testkogu klassifitseerimise eksimismaatriksi joonis.
3. feature_importance_sum.csv ja all_features_sum_bar_plot.png - kõigi tunnuste ajaliselt summeeritud olulisuse andmed ja joonis.
4. feature_importance_mean.csv ja all_features_mean_bar_plot.png - kõigi tunnuste keskmise olulisuse andmed ja joonis. 
7. all_features_in_time.png ja top5_features_in_time.png - kõigi ja 5 kõige olulisema tunnuse olulisus päevade lõikes (joonised).
8. /plots kaust - iga tunnuse olulisused eraldi päevade lõikes (joonised).
9. RandomizedSearcvCV_result.csv - hüperparameetrite testimise tulemused. 