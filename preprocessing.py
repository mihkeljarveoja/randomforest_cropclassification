import csv
from datetime import datetime
import psycopg2
import pandas as pd
import numpy as np

doy_list = list(range(121, 244)) # May 1st - August 31st

# if needed to test only with test_parcels, then add "AND parcel_id IN {}" to sql queries and read all queries like this:
# df_temperature_prelim = pd.read_sql_query(temperature_recordv_query.format(test_parcels), con)
test_parcels = '(1, 2, 431, 10, 11, 1573, 11284, 6639, 136, 161924, 4011, 264, 950, 103201, 5535, 4649, 6542, 10670, 11099, 13343)'



# resample and interpolate timeseries of each parcel
def resample_interpolate_timeseries (input_dataframe, doy_list):
    print('Starting resampling and interpolating...')
    unique_parcels = input_dataframe.parcel_id.unique()  # list of unique parcels id-s
    parameters = list(input_dataframe.columns.values)[2:]  # list of all parameters
    all_days = pd.DataFrame(doy_list, index=doy_list, columns=["doy"])  # creating df with all wished dates
    processed_parcels_list = [None]*len(unique_parcels)  # list where all processed parcels are collected before they are put together again
    counter = 1  # counter for displaying progress
    index = 0  # index creator for bad parcels
    bad_parcels_idxs = []  # list for parcels (indexes), where are too few time series points

    # iterate over all unique parcels
    for parcel in unique_parcels:
        # separating one parcel from dataframe
        df_parcel = input_dataframe.loc[input_dataframe['parcel_id'] == parcel]
        # merging parcel doy-s with all doy-s
        df_parcel_norm = all_days.merge(df_parcel, how="left", on="doy")
        # filling parcel_id NA values (formed during the merge) with parcel_id values
        df_parcel_norm['parcel_id'].fillna(parcel, inplace=True)

        # iterate over all parameters regarding this parcel
        for param in parameters:
            # only if there are more than x time series points
            if df_parcel_norm[param].count() > 9:
                # interpolating missing values in the middle of timeseries
                df_parcel_norm[param].interpolate(method="slinear", inplace=True)
                # filling empty values in the beginning and end of timeseries with 0
                df_parcel_norm[param].fillna(0, inplace=True)

            else: # if there are less than x time series points, parcel will be logged and indexes of "bad parcels" gathered
                with open('/home/mihkel.jarveoja/util/rita_RF/2019_output/processing_log.csv', 'a') as file:
                    # writing parcels with too few time series points to log
                    file.write('Skipping parameter: {} for parcel: {}. Ts points only: {}'.format(param, parcel, df_parcel_norm[param].count()))
                    file.write('\n')
                print('Skipping parameter: {} for parcel: {}. Ts points only: {}'.format(param, parcel, df_parcel_norm[param].count()))
                bad_parcels_idxs.append(index) # appending parcel's index to "bad parcels" list
                break

        #gather all temporary dataframes into list
        processed_parcels_list[index] = df_parcel_norm

        print('{}/{} parcels processed'.format(counter, len(unique_parcels)))
        counter += 1
        index += 1

    processed_parcels_list_clean = []  # new list, where only valid parcels will be added
    for index, item in enumerate(processed_parcels_list):
        if index not in bad_parcels_idxs:  # checking, if the processed parcel is not in "bad parcels" list
            processed_parcels_list_clean.append(item)

    # concatenate processed parcels list into df
    df_interpolated = pd.concat(processed_parcels_list_clean)
    print('Resampling and interpolating finished...')
    return df_interpolated

# format time series dataframe into suitable format for sklearn
def prepare_ts_for_model (input_ts_dataframe, param_prefix):
    print('Changing dataframe structure...')
    parameters = list(input_ts_dataframe.columns.values)[2:]  # list of all parameters
    processed_param_list = [None]*len(parameters)
    index = 0
    counter = 1  # counter for displaying progress

    if len(parameters) > 1:
        print('Preparing {} time series for model...'.format(param_prefix))
        # iterate over each parameter (column)
        for parameter in parameters:
            df_temp = input_ts_dataframe.pivot(index='parcel_id', columns='doy', values=parameter)  # changing df structure: parcels as index, doys as columns
            df_temp.columns = df_temp.columns.astype(int)  # column names into integers
            df_temp = df_temp.add_prefix('{}_{}_'.format(param_prefix, parameter))  # adding parameter name prefix to each doy column
            processed_param_list[index] = df_temp  # gather all temporary dataframes into list
            index += 1
            print('{}/{} parameters processed'.format(counter, len(parameters)))
            counter += 1

        # concatenate processed parcels list into df
        df_output = pd.concat(processed_param_list, axis='columns', join='inner', sort=False)
    else:
        print('Preparing {} time series for model...'.format(param_prefix))
        df_temp = input_ts_dataframe.pivot(index='parcel_id', columns='doy', values=parameters[0])
        df_temp.columns = df_temp.columns.astype(int)
        df_output = df_temp.add_prefix(param_prefix)

    print('Dataframe re-structured...')
    return df_output

try: # connect to database
    con = psycopg2.connect(user = "username",
                            password = "password",
                            host = "127.0.0.1",
                            port = "port",
                            database = "database_name")

    cur = con.cursor()

    # sql queries to get data from database
    parcelv_sql_query = "SELECT parcel_id, crop_code, x_norm_loc, y_norm_loc FROM parcelv_narrow;"
    soil_recordv_query = "SELECT parcel_id, soil_type4 FROM soil_recordv"
    temperature_recordv_query = "SELECT parcel_id, temperature_product_stop_date::date AS doy, temperature_mean " \
                                "FROM temperature_recordv WHERE temperature_product_stop_date >= '2019-05-01' AND " \
                                "temperature_product_stop_date < '2019-09-01' ;"
    s1_ts_recordv_query = "SELECT parcel_id, s1_product_stop_date::date AS doy, " \
                          "cohvh_median, cohvv_median, s0vh_median, s0vv_median, vhvv_median " \
                          "FROM s1_ts_recordv " \
                          "WHERE s1_product_stop_date >= '2019-05-01' AND s1_product_stop_date < '2019-09-01' ;"
    s2_ts_recordv_query = "SELECT parcel_id, s2_product_start_date::date AS doy, " \
                          "b2_median, b3_median, b4_median, b5_median, b6_median, b7_median, b8_median, b8a_median, b11_median, b12_median, " \
                          "ndvi_median, ndwi_median, ndvire_median, tc_wetness_median, tc_vegetation_median, tc_brightness_median, misra_yellow_vegetation_median, psri_median, wri_median " \
                          "FROM s2_ts_recordv " \
                          "WHERE s2_product_start_date >= '2019-05-01' AND s2_product_start_date < '2019-09-01' ;"
    precip_ts_3h_recordv_query = "SELECT parcel_id, precipitation_product_stop_date::date AS doy, " \
                          "precipitation_sum_mean " \
                          "FROM precipitation_recordv " \
                          "WHERE precipitation_product_stop_date >= '2019-05-01' AND precipitation_product_stop_date < '2019-09-01' " \
                          "AND sum_duration = '3h';"
    precip_ts_24h_recordv_query = "SELECT parcel_id, precipitation_product_stop_date::date AS doy, " \
                                 "precipitation_sum_mean " \
                                 "FROM precipitation_recordv " \
                                 "WHERE precipitation_product_stop_date >= '2019-05-01' AND precipitation_product_stop_date < '2019-09-01' " \
                                 "AND sum_duration = '24h';"

    # open and start log
    with open('/home/mihkel.jarveoja/util/rita_RF/2019_output/processing_log.csv', 'w') as file:
        file.write('Starting importing data from db! Current time: {}'.format(datetime.now()))
        file.write('\n' + '--------------------------------' + '\n')

    # data import from database
    print('Importing parcels from db ...')
    df_parcels = pd.read_sql_query(parcelv_sql_query, con, index_col='parcel_id')
    print('>>>>>Sorting parcels by id ...')
    df_parcels = df_parcels.sort_values(by=['parcel_id'])
    print('Importing soil from db ...')
    df_soil = pd.read_sql_query(soil_recordv_query, con, index_col='parcel_id')

    print('Importing temperature from db ...')
    df_temperature_prelim = pd.read_sql_query(temperature_recordv_query, con)
    df_temperature_prelim['doy'] = pd.to_datetime(df_temperature_prelim['doy'], utc=True).dt.dayofyear

    print('Importing 3h precipitation from db ...')
    df_precip_3h_prelim = pd.read_sql_query(precip_ts_3h_recordv_query, con)
    df_precip_3h_prelim['doy'] = pd.to_datetime(df_precip_3h_prelim['doy'], utc=True).dt.dayofyear

    print('Importing 24h precipitation from db ...')
    df_precip_24h_prelim = pd.read_sql_query(precip_ts_24h_recordv_query, con)
    df_precip_24h_prelim['doy'] = pd.to_datetime(df_precip_24h_prelim['doy'], utc=True).dt.dayofyear

    print('Importing S1 from db ...')
    df_s1_prelim = pd.read_sql_query(s1_ts_recordv_query, con)
    df_s1_prelim['doy'] = pd.to_datetime(df_s1_prelim['doy'], utc=True).dt.dayofyear

    print('Importing S2 from db ...')
    df_s2_prelim = pd.read_sql_query(s2_ts_recordv_query, con)
    df_s2_prelim['doy'] = pd.to_datetime(df_s2_prelim['doy'], utc=True).dt.dayofyear

    with open('/home/mihkel.jarveoja/util/rita_RF/2019_output/processing_log.csv', 'a') as file:
        file.write('Importing data from db done. Starting preprocessing! Current time: {}'.format(datetime.now()))
        file.write('\n' + '--------------------------------' + '\n')

    print('----------------')
    print('Starting preprocessing for model...')
    print('----------------')

    # data preprocessing (resampling, interpolating etc)
    print('>>>>Processing S2 ...')
    df_s2 = prepare_ts_for_model(resample_interpolate_timeseries(df_s2_prelim, doy_list), 's2')
    if df_s2.isnull().values.any():
        with open('/home/mihkel.jarveoja/util/rita_RF/2019_output/processing_log.csv', 'a') as file:
            file.write('NaN values in df_s2!')
            file.write('\n')
    print('----------------')
    print('>>>>Processing S1 ...')
    df_s1 = prepare_ts_for_model(resample_interpolate_timeseries(df_s1_prelim, doy_list), 's1')
    if df_s1.isnull().values.any():
        with open('/home/mihkel.jarveoja/util/rita_RF/2019_output/processing_log.csv', 'a') as file:
            file.write('NaN values in df_s1!')
            file.write('\n')
    print('----------------')
    print('>>>>Processing temperature ...')
    df_temperature = prepare_ts_for_model(resample_interpolate_timeseries(df_temperature_prelim, doy_list), 'temp_mean_')
    if df_temperature.isnull().values.any():
        with open('/home/mihkel.jarveoja/util/rita_RF/2019_output/processing_log.csv', 'a') as file:
            file.write('NaN values in df_temperature!')
            file.write('\n')
    print('----------------')
    print('>>>>Processing 3h precipitation ...')
    df_precip_3h = prepare_ts_for_model(df_precip_3h_prelim, 'precip_mean_3h_').fillna(0)

    if df_precip_3h.isnull().values.any():
        with open('/home/mihkel.jarveoja/util/rita_RF/2019_output/processing_log.csv', 'a') as file:
            file.write('NaN values in df_precip_3h!')
            file.write('\n')
    print('----------------')
    print('>>>>Processing 24h precipitation ...')
    df_precip_24h = prepare_ts_for_model(df_precip_24h_prelim, 'precip_mean_24h_').fillna(0)
    if df_precip_24h.isnull().values.any():
        with open('/home/mihkel.jarveoja/util/rita_RF/2019_output/processing_log.csv', 'a') as file:
            file.write('NaN values in df_precip_24h!')
            file.write('\n')

    print('Combining all dataframes into one ...')
    print('Concatenating precipitation together')
    df_precip = pd.concat([df_precip_3h, df_precip_24h], axis='columns', join='inner')
    if df_precip.isnull().values.any():
        print('Error! There are NaN values in dataframe')

    print('Concatenating temperature and precipitation together')
    df_temp_precip = pd.concat([df_temperature, df_precip], axis='columns', join='inner')
    if df_temp_precip.isnull().values.any():
        print('Error! There are NaN values in dataframe')

    print('Merging parcels and soil')
    df_parcels = df_parcels.merge(df_soil, how='inner', on='parcel_id')
    if df_parcels.isnull().values.any():
        print('Error! There are NaN values in dataframe')

    print('Merging parcels, soil, temperature and precipitation')
    df_parcels = pd.concat([df_parcels, df_temp_precip], axis='columns', join='inner')
    if df_parcels.isnull().values.any():
        print('Error! There are NaN values in dataframe')

    print('Merging parcels and S1')
    df_parcels = pd.concat([df_parcels, df_s1], axis='columns', join='inner')
    if df_parcels.isnull().values.any():
        print('Error! There are NaN values in dataframe')

    print('Merging parcels and S2')
    df_parcels = pd.concat([df_parcels, df_s2], axis='columns', join='inner')
    if df_parcels.isnull().values.any():
        print('Error! There are NaN values in dataframe')

    y = df_parcels.crop_code.astype(int).to_numpy()  #labels (code) for machine learing dataset
    df_parcels = df_parcels.drop(columns=['crop_code'])
    df_parcels_params = list(df_parcels.columns.values)  # parameter names for machine learning dataset

    with open ('/home/mihkel.jarveoja/util/rita_RF/2019_output/train_set_headers.csv', 'w', newline='') as csvfile:
        wr = csv.writer(csvfile)
        wr.writerow(df_parcels_params)

    X = df_parcels.to_numpy(dtype='float')  # whole dataset for machine learning

    # saving the whole dataset and labels to disk in .npz format
    np.savez_compressed('/home/mihkel.jarveoja/util/rita_RF/2019_output/train_set', x=X, y=y)

    with open('/home/mihkel.jarveoja/util/rita_RF/2019_output/processing_log.csv', 'a') as file:
        file.write('Preprocessing finished! Current time: {}'.format(datetime.now()))
        file.write('\n')

except (Exception, psycopg2.Error) as error:
    print("Error while connecting to PostgreSQL", error)
finally:
    if (con):
        cur.close()
        con.close()
        print("PostgreSQL connection is closed")