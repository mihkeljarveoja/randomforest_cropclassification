import os
import csv
import matplotlib.pyplot as plt
from datetime import datetime
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split, RandomizedSearchCV
from sklearn.metrics import confusion_matrix, classification_report
import pandas as pd
import numpy as np
import seaborn as sns

narrow_labels_dict = {1: 'Aedmaasikas', 2: 'Heintaimed, kõrrelised', 3: 'Liblikõieliste segud (alla 80%)', 4: 'Liblikõielised (üle 80%)', 5: 'Kanep', 6: 'Kartul',
               7: 'Põldhernes', 8: 'Põlduba', 9: 'Muu kaunvili', 10: 'Mais', 11: 'Astelpaju', 12: 'Marjapõõsad ja viljapuud', 13: 'Mustkesa',
               14: 'Peakapsas', 15: 'Porgand', 16: 'Punapeet', 17: 'Suviraps ja -rüps', 18: 'Suvinisu ja speltanisu', 19: 'Suvioder', 20: 'Kaer', 21: 'Tatar',
               22: 'Taliraps ja -rüps', 23: 'Rukis', 24: 'Talinisu', 25: 'Talioder', 26: 'Talitritikale', 27: 'Muu teravili', 28: 'Muu'}

all_model_features = ['x_norm_loc', 'y_norm_loc', 'soil_type4', 'temp_mean', 'precip_mean_3h', 'precip_mean_24h',
                  's1_cohvh_median', 's1_cohvv_median', 's1_s0vh_median', 's1_s0vv_median', 's1_vhvv_median',
                  's2_b2_median', 's2_b3_median', 's2_b4_median', 's2_b5_median', 's2_b6_median', 's2_b7_median',
                  's2_b8_median', 's2_b8a_median', 's2_b11_median', 's2_b12_median', 's2_ndvi_median', 's2_ndwi_median',
                  's2_ndvire_median', 's2_tc_wetness_median', 's2_tc_vegetation_median', 's2_tc_brightness_median',
                  's2_misra_yellow_vegetation_median', 's2_psri_median', 's2_wri_median']

color_dict_for_plot = dict({'x_norm_loc':'black', 'y_norm_loc':'black', 'soil_type4':'brown', 'temp_mean':'darkorchid', 'precip_mean_3h':'darkorchid', 'precip_mean_24h':'darkorchid',
                  's1_cohvh_median':'mediumturquoise', 's1_cohvv_median':'mediumturquoise', 's1_s0vh_median':'mediumturquoise', 's1_s0vv_median':'mediumturquoise', 's1_vhvv_median':'mediumturquoise',
                  's2_b2_median':'lightsalmon', 's2_b3_median':'lightsalmon', 's2_b4_median':'lightsalmon', 's2_b5_median':'lightsalmon', 's2_b6_median':'lightsalmon', 's2_b7_median':'lightsalmon',
                  's2_b8_median':'lightsalmon', 's2_b8a_median':'lightsalmon', 's2_b11_median':'lightsalmon', 's2_b12_median':'lightsalmon', 's2_ndvi_median':'lightsalmon', 's2_ndwi_median':'lightsalmon',
                  's2_ndvire_median':'lightsalmon', 's2_tc_wetness_median':'lightsalmon', 's2_tc_vegetation_median':'lightsalmon', 's2_tc_brightness_median':'lightsalmon',
                  's2_misra_yellow_vegetation_median':'lightsalmon', 's2_psri_median':'lightsalmon', 's2_wri_median':'lightsalmon'})

color_dict2_for_plot = dict({'s1_s0vh_median':'turquoise', 's1_s0vv_median':'darkcyan', 's2_ndvi_median':'lightcoral', 's2_tc_vegetation_median':'red', 's2_psri_median':'darkred'})

# parameter controlling if hyperparameters optimizing is run
run_param_comparison = False

# function for plotting confusion matrix
def plot_conf_matrix(confusion_matrix):
    path_output = '/home/mihkel.jarveoja/util/rita_RF/2019_output'
    conf_matrix = pd.DataFrame(data=confusion_matrix, index=list(narrow_labels_dict.keys()), columns=list(narrow_labels_dict.keys()))
    fig, ax = plt.subplots(figsize=(20, 20))
    ax.imshow(conf_matrix, interpolation='nearest', cmap=plt.cm.Blues)
    codes = list(conf_matrix.index)
    labels = [str(x) + '-' + narrow_labels_dict[x] for x in codes]

    ax.set(xticks=np.arange(conf_matrix.shape[1]),
                   yticks=np.arange(conf_matrix.shape[0]),
                   xticklabels=labels+codes, yticklabels=labels+codes)
    ax.set_title('Eksimismaatriks 2019: RandomForest klassifitseerija', pad=30, fontsize=25)
    plt.xlabel('Ennustatud klass', fontsize=20)
    plt.ylabel('Tegelik klass',fontsize=20)
    for edge, spine in ax.spines.items():
                spine.set_visible(False)
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
                     rotation_mode="anchor", fontsize=20)
    plt.setp(ax.get_yticklabels(), fontsize=20)
    fmt = '.2f'
    thresh = conf_matrix.values.max() / 2.
    for i in range(len(codes)):
        I = codes[i]
        for j in range(len(codes)):
            J = codes[j]
            ax.text(i, j, format(conf_matrix[I][J], fmt),
                    ha="center", va="center",
                    color="white" if conf_matrix[I][J] > thresh or conf_matrix[I][J] < 0.01 else "black", fontsize=15)
    fig.tight_layout()
    plt.savefig(os.path.join(path_output, 'confusion_matrix.png'))
    plt.close()

# function to load fitting data from disk
def load_fitting_data ():
    print('Loading data from files...')
    # loading the data from preprocessed file, 2D numpy array
    data_from_file = np.load('/home/mihkel.jarveoja/util/rita_RF/2019_output/train_set.npz')
    return data_from_file

# loading feature names (column headers) from file
def load_feature_names():
    with open('/home/mihkel.jarveoja/util/rita_RF/2019_output/train_set_headers.csv', 'r', newline='') as csvfile:
        r = csv.reader(csvfile, delimiter=',')
        headers = list(r)[0]
    print('Feature names loaded!')
    return headers

# function to calculate, reorganize and save feature importances into df
def calculate_feature_importance(feature_dict):
    x = pd.DataFrame.from_dict(feature_dict, orient='index').rename(columns={0: 'importance'})  # creating dataframe from dictionary
    x.insert(0, 'doy', np.nan)  # inserting new columns to df
    x.insert(0, 'feature', np.nan)
    # filling the rows of df with
    for index, row in x.iterrows():
        if index == 'x_norm_loc':
            x.loc[index] = [index, 0, row['importance']]
            continue
        if index == 'y_norm_loc':
            x.loc[index] = [index, 0, row['importance']]
            continue
        if index == 'soil_type4':
            x.loc[index] = [index, 0, row['importance']]
            continue
        else:
            x.loc[index] = [index.rsplit('_', 1)[0], index.rsplit('_', 1)[1], row['importance']]  # slicing strings and adding them to correct columns

    x.reset_index(drop=True, inplace=True)  # resetting index and deleting old index (long feat names)
    importance_sum = x.groupby(['feature']).sum().sort_values(by=['importance'], ascending=False)  # grouping by feature, calculating sum and sorting values by importances
    importance_mean = x.groupby(['feature']).mean().sort_values(by=['importance'], ascending=False)  # grouping by feature, calculating mean and sorting values by importances
    print(importance_sum)
    print(importance_mean)
    importance_sum.to_csv('/home/mihkel.jarveoja/util/rita_RF/2019_output/feature_importance_sum.csv', index=True)
    importance_mean.to_csv('/home/mihkel.jarveoja/util/rita_RF/2019_output/feature_importance_mean.csv', index=True)
    return x  # returning reorganized dataframe, where doy, feature_name and importances are all in separate columns

# plotting features summarized importances to bar plot
def plot_importance_sum(df):
    importance_sum = df.groupby(['feature']).sum().sort_values(by=['importance'], ascending=False)
    fig, ax = plt.subplots(figsize=(40, 20))
    sns.barplot(x='feature', y='importance', data=df, order=importance_sum.index, estimator=sum, ci=None, palette=color_dict_for_plot)
    ax.set_title('Kõik tunnused (ajaliselt summeeritud), 2019', pad=30, fontsize=40)
    plt.setp(ax.get_xticklabels(), rotation=90)
    plt.xlabel('Tunnus', fontsize=35)
    plt.ylabel('Olulisus', fontsize=35)
    plt.setp(ax.get_yticklabels(), fontsize=25)
    plt.setp(ax.get_xticklabels(), fontsize=25)
    fig.tight_layout()
    plt.savefig('/home/mihkel.jarveoja/util/rita_RF/2019_output/all_features_sum_bar_plot.png')
    plt.close()

# plotting features mean importances to bar plot
def plot_importance_mean(df):
    importance_mean = df.groupby(['feature']).mean().sort_values(by=['importance'], ascending=False)
    fig, ax = plt.subplots(figsize=(40, 20))
    sns.barplot(x='feature', y='importance', data=df, order=importance_mean.index, ci=None, palette=color_dict_for_plot)
    ax.set_title('Kõik tunnused (keskmised), 2019', pad=30, fontsize=40)
    plt.setp(ax.get_xticklabels(), rotation=90)
    plt.xlabel('Tunnus', fontsize=35)
    plt.ylabel('Olulisus', fontsize=35)
    plt.setp(ax.get_yticklabels(), fontsize=25)
    plt.setp(ax.get_xticklabels(), fontsize=25)
    fig.tight_layout()
    plt.savefig('/home/mihkel.jarveoja/util/rita_RF/2019_output/all_features_mean_bar_plot.png')
    plt.close()

# plotting and saving selected feature importance in timeline
def plot_feat_importance_intime(feature, importances_df):
    df_feat_importance = importances_df.loc[importances_df['feature'] == feature]
    fig, ax = plt.subplots(figsize=(40, 20))
    sns.lineplot(x='doy', y='importance', data=df_feat_importance, marker='.')
    ax.xaxis.set_major_locator(plt.MaxNLocator(40))
    ax.grid(True)
    ax.set_title(feature + '_2019', pad=30, fontsize=40)
    plt.xlabel('Päev', fontsize=35)
    plt.ylabel('Olulisus', fontsize=35)
    plt.setp(ax.get_yticklabels(), fontsize=25)
    plt.setp(ax.get_xticklabels(), fontsize=25)
    plt.setp(ax.lines, linewidth=3, markersize=15)
    fig.tight_layout()
    plt.savefig(os.path.join('/home/mihkel.jarveoja/util/rita_RF/2019_output/plots/', feature + '.png'))
    plt.close()

# plotting all features on one plot
def plot_all_feat_importance_intime(df):
    fig, ax = plt.subplots(figsize=(40, 20))
    g = sns.lineplot(x='doy', y='importance', hue='feature', data=df, marker=None, palette=color_dict_for_plot, legend=False)
    ax.xaxis.set_major_locator(plt.MaxNLocator(40))
    ax.grid(True)
    plt.setp(g.lines, alpha=.5)
    ax.set_title('Kõik tunnused, 2019', pad=30, fontsize=40)
    plt.xlabel('Päev', fontsize=35)
    plt.ylabel('Olulisus', fontsize=35)
    plt.setp(ax.get_yticklabels(), fontsize=25)
    plt.setp(ax.get_xticklabels(), fontsize=25)
    plt.setp(ax.lines, linewidth=3, markersize=15)
    fig.tight_layout()
    plt.savefig('/home/mihkel.jarveoja/util/rita_RF/2019_output/all_features_in_time.png')
    plt.close()

# plotting selection of features on one plot
def plot_selected_feat_importance_intime(df, selection):
    fig, ax = plt.subplots(figsize=(40, 20))
    g = sns.lineplot(x='doy', y='importance', hue='feature', hue_order=selection, data=df, marker=None, palette=color_dict2_for_plot, legend='brief')
    ax.xaxis.set_major_locator(plt.MaxNLocator(40))
    ax.grid(True)
    ax.set_title('Oluliseimad tunnused, 2019', pad=30, fontsize=40)
    plt.xlabel('Päev', fontsize=35)
    plt.ylabel('Olulisus', fontsize=35)
    plt.setp(ax.get_yticklabels(), fontsize=25)
    plt.setp(ax.get_xticklabels(), fontsize=25)
    plt.setp(ax.lines, linewidth=3, markersize=15)
    plt.setp(ax.get_legend().get_texts(), fontsize=25)
    plt.setp(ax.get_legend().get_title(), fontsize=30)
    fig.tight_layout()
    plt.savefig('/home/mihkel.jarveoja/util/rita_RF/2019_output/top5_features_in_time.png')
    plt.close()


print('Script started!')
start_time = str(datetime.now())
print('--------------------')
X = load_fitting_data()['x']  # whole dataset without labels
y = load_fitting_data()['y']  # labels

print('Shape of the whole dataset: {}'.format(X.shape))
print('Shape of the labels dataset: {}'.format(y.shape))
print('Number of parcels in the the whole dataset: {}'.format(X.shape))
print('--------------------')

# splitting the dataset into train and test sets (labels as well)
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, train_size=0.8, random_state=36, shuffle=True)

print('Shape of the train set: {}'.format(X_train.shape))
print('Number of parcels in train set: {}'.format(len(X_train)))
print('--------------------')
print('Shape of the test set: {}'.format(X_test.shape))
print('Number of parcels in test set: {}'.format(len(X_test)))
print('--------------------')

with open('/home/mihkel.jarveoja/util/rita_RF/2019_output/classification_report.txt', 'w') as file:
    file.write('Script start time: {}'.format(start_time) + '\n')
    file.write('Shape of the whole dataset: {}'.format(X.shape) + '\n')
    file.write('Shape of the labels dataset: {}'.format(y.shape) + '\n')
    file.write('Number of parcels in the the whole dataset: {}'.format(len(X)) + '\n')
    file.write('--------------------------------' + '\n')
    file.write('Shape of the train set: {}'.format(X_train.shape) + '\n')
    file.write('Number of parcels in train set: {}'.format(len(X_train)) + '\n')
    file.write('--------------------------------' + '\n')
    file.write('Shape of the test set: {}'.format(X_test.shape) + '\n')
    file.write('Number of parcels in test set: {}'.format(len(X_test)) + '\n')
    file.write('--------------------------------' + '\n')
    file.write('\n')

# hyperparameter comparison test parameters
param_grid = {
    'n_estimators': np.linspace(100, 500, 5).astype(int),
    'criterion': ['gini'],
    'max_features': ['sqrt', 'log2'],
    'max_leaf_nodes': [None] + list(np.linspace(100, 500, 5).astype(int)),
    'min_samples_split': [2, 5, 10],
    'bootstrap': [True, False]}

# hyperparameters comparsion test
if run_param_comparison:
    estimator = RandomForestClassifier(random_state = 42, n_jobs=10, max_depth=None)
    rs = RandomizedSearchCV(estimator, param_grid, n_jobs=10, scoring='f1_weighted', cv = 3, n_iter=20, verbose=2, random_state=42, refit=True, error_score='raise', return_train_score=True)
    rs.fit(X_train, y_train)
    print(rs.best_params_)
    print(rs.best_index_)
    print(rs.best_estimator_)
    table = pd.DataFrame.from_dict(rs.cv_results_)
    table.to_csv('/home/mihkel.jarveoja/util/rita_RF/2019_output/RandomizedSearcvCV_result.csv', sep=';', mode='w')

# creating the classifier
forest = RandomForestClassifier(n_estimators=300, max_features='log2', max_depth=None, min_samples_split=2, max_leaf_nodes=None, bootstrap=False, n_jobs=-1, random_state=42, class_weight=None, verbose=1)

# fitting the model
print(forest.fit(X_train,y_train))

# predicting on test set
y_pred = forest.predict(X_test).tolist()

# creating and printing classification report
cl_report = classification_report(y_test, y_pred)

print(cl_report)

# write classification report to file
with open('/home/mihkel.jarveoja/util/rita_RF/2019_output/classification_report.txt', 'a') as file:
    file.write(cl_report)
    file.write('--------------------------------' + '\n')
    file.write('\n')

# creating confusion matrix csv
cm_normalized = confusion_matrix(y_test, y_pred, normalize='true') # normalized values
cm = confusion_matrix(y_test, y_pred, normalize=None) #raw values
np.savetxt('/home/mihkel.jarveoja/util/rita_RF/2019_output/confusion_matrix_norm.csv', cm_normalized, delimiter=',')
np.savetxt('/home/mihkel.jarveoja/util/rita_RF/2019_output/confusion_matrix.csv', cm, delimiter=',')

# getting feature importances
importances = forest.feature_importances_
feats = {}
for feature, importance in zip(load_feature_names(), importances):
    feats[feature] = importance

importances_df = calculate_feature_importance(feats)
plot_importance_sum(importances_df)
plot_importance_mean(importances_df)
plot_all_feat_importance_intime(importances_df)

selected_feat = ['s1_s0vh_median', 's1_s0vv_median', 's2_ndvi_median', 's2_tc_vegetation_median', 's2_psri_median']
plot_selected_feat_importance_intime(importances_df, selected_feat)

# plotting confusion matrix
plot_conf_matrix(cm_normalized)

for feat in all_model_features:
    plot_feat_importance_intime(feat, importances_df)

with open('/home/mihkel.jarveoja/util/rita_RF/2019_output/classification_report.txt', 'a') as file:
    file.write('Script end time: {}'.format(datetime.now()))
    file.write('\n')